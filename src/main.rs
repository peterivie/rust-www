// #![deny(warnings)]

use std::env;
use std::thread;
use std::time::Duration;


/// Provides a RESTful web server managing some Events.
///
/// API will be:
///
/// - `POST /events`: create a new Event.
#[tokio::main]
async fn main() {
    use std::sync::{Arc};
    use std::sync::atomic::{AtomicBool, Ordering};
    use models::Event;

    if env::var_os("RUST_LOG").is_none() {
        env::set_var("RUST_LOG", "events=debug");
    }
    pretty_env_logger::init();

    let running = Arc::new(AtomicBool::new(true));
    let r = running.clone();

    let db1 = models::blank_db1();
    
    let persistence_handler = thread::spawn(move || {
        
        // let mut events = db1.clone().lock();
        
        while running.load(Ordering::SeqCst) {

            // let next_event = events.pop_front();
            let next_event: std::option::Option<Event> = None;
            
            if !next_event.is_none() {
                println!("Event found: {:?}", next_event);
                thread::sleep(Duration::from_millis(2000));
            } else if !running.load(Ordering::SeqCst) {
                break;
            } else {
                println!("...");
            }
            thread::sleep(Duration::from_millis(1000));
        }
        println!("Gracefully shut down!");
    });

    let routes = filters::events_create(db1);
    let (_addr, fut2) = warp::serve(routes)
        .bind_with_graceful_shutdown(([127, 0, 0, 1], 3030), async move {
            tokio::signal::ctrl_c()
                .await
                .expect("failed to listen to shutdown signal");
            println!("Gracefully shutting down...");
            r.store(false, Ordering::SeqCst);
        });
    fut2.await;
    persistence_handler.join().unwrap();
}

mod filters {
    use super::handlers;
    use super::models::Db1;
    use warp::Filter;

    /// POST /events with JSON body
    pub fn events_create(
        db: Db1,
    ) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        warp::post()
            .and(warp::path::param::<String>())
            .and(warp::filters::body::bytes())
            .and(with_db1(db))
            .and_then(handlers::create_event)
    }

    pub fn with_db1(db: Db1) -> impl Filter<Extract = (Db1,), Error = std::convert::Infallible> + Clone {
        warp::any().map(move || db.clone())
    }
}

mod handlers {
    use super::models::{Db1, Event};
    use std::convert::Infallible;
    use warp::http::StatusCode;
    use bytes::Bytes;

    const ENTRY_TIMESTAMP_FORMAT: &str = "%Y-%m-%dT%T%.6f";

    pub async fn create_event(action: String, bytes: Bytes, db: Db1) -> Result<impl warp::Reply, Infallible> {
        let body = String::from_utf8(bytes.to_vec()).unwrap();
        use chrono::{Utc};
        let nanots = Utc::now().format(ENTRY_TIMESTAMP_FORMAT).to_string();

        println!("action = {:?}", action);
        println!("nanots = {:?}", nanots);
        println!("body = {:?}", body);
        
        let mut events = db.lock().await;
        events.push_back(Event{action, nanots, body});

        Ok(StatusCode::CREATED)
    }
}

mod models {
    use serde_derive::{Deserialize, Serialize};
    use std::sync::Arc;
    use tokio::sync::Mutex;
    use std::collections::VecDeque;

    pub type Db1 = Arc<Mutex<VecDeque<Event>>>;

    pub fn blank_db1() -> Db1 {
        Arc::new(Mutex::new(VecDeque::new()))
    }

    #[derive(Debug, Deserialize, Serialize, Clone)]
    pub struct Event {
        pub action: String,
        pub nanots: String,
        pub body: String,
    }
}

// #[cfg(test)]
// mod tests {
//     use warp::http::StatusCode;
//     use warp::test::request;

//     use super::{
//         filters,
//         models::{self, Todo},
//     };

//     #[tokio::test]
//     async fn test_post() {
//         let db = models::blank_db();
//         let api = filters::todos(db);

//         let resp = request()
//             .method("POST")
//             .path("/todos")
//             .json(&todo1())
//             .reply(&api)
//             .await;

//         assert_eq!(resp.status(), StatusCode::CREATED);
//     }

//     #[tokio::test]
//     async fn test_post_conflict() {
//         let db = models::blank_db();
//         db.lock().await.push(todo1());
//         let api = filters::todos(db);

//         let resp = request()
//             .method("POST")
//             .path("/todos")
//             .json(&todo1())
//             .reply(&api)
//             .await;

//         assert_eq!(resp.status(), StatusCode::BAD_REQUEST);
//     }

//     #[tokio::test]
//     async fn test_put_unknown() {
//         let _ = pretty_env_logger::try_init();
//         let db = models::blank_db();
//         let api = filters::todos(db);

//         let resp = request()
//             .method("PUT")
//             .path("/todos/1")
//             .header("authorization", "Bearer admin")
//             .json(&todo1())
//             .reply(&api)
//             .await;

//         assert_eq!(resp.status(), StatusCode::NOT_FOUND);
//     }

//     fn todo1() -> Todo {
//         Todo {
//             id: 1,
//             text: "test 1".into(),
//             completed: false,
//         }
//     }
// }